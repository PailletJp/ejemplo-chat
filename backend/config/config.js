require("dotenv").config();
// TODO: Crear .env
const dbHost = '127.0.0.1';
const dbName =  'socket';
const dbUsername = 'root';
const dbPassword = '';
const dbPort = process.env.DB_PORT || 5432;

module.exports = {
  development: {
    username: dbUsername,
    password: dbPassword,
    database: dbName,
    host: dbHost,
    dialect: "mysql",
  },
  test: {
    username: dbUsername,
    password: dbPassword,
    database: "twitter_automator_test",
    host: dbHost,
    port: dbPort,
    dialect: "postgres",
  },
  production: {
    use_env_variable: "DATABASE_URL",
    username: dbUsername,
    password: dbPassword,
    database: dbName,
    host: dbHost,
    port: dbPort,
    dialect: "postgres",
  },
};
